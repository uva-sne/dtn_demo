/*
This is a component of the DTN Agent Demo. It will run a WebSocket server
allowing sources and monitors to connect to it. It will process the information
received from ths sources and send it to the monitor to be displayed. The config
file provides the IP address and port the server will listen on. The config file
also specifies the default interval the sources should use to send data. The
monitor may change this interval.
*/
const WebSocket = require('ws');

class AgentDemoSink{
  constructor(config_file_name) {
    this.config = require('./config/' + config_file_name);
    this.wss = null;
    //this.data_sources = new Set();
    //this.event_sources = new Set();
    this.monitors = [];
    this.sources = {};
    this.callbacks = {};
    this.sockets = {};
  }

  init() {
    this.registerMessageHandlers();
    this.initWebSocketServer();
  }

  initWebSocketServer() {
    let options = { host: this.config.host, port: this.config.port };
    this.wss = new WebSocket.Server(options);
    this.wss.on('listening',  ()                => this.listeningHandler());
    this.wss.on('connection', (socket, request) => this.connectionHandler(socket, request));
    this.wss.on('close',      ()                => this.closeHandler());
    this.wss.on('error',      (error)           => this.errorHandler(error));
  }

/* Web Socket Server Event Handlers */

  listeningHandler() {
    console.log('[WSS-LISTENING]', this.config.host, this.config.port);
  }

  closeHandler() {
    console.log('[WSS-CLOSE]');
  }

  errorHandler(error) {
    console.log('[WSS-ERROR]', error);
  }

  connectionHandler(ws, request) {
    console.log('[WSS-CONNECTION]', request.connection.remoteAddress, request.connection.remotePort);
    ws.on('open',    ()             => this.wsOpenHandler()                 );
    ws.on('message', (data)         => this.wsMessageHandler(ws, data)      );
    ws.on('close',   (code, reason) => this.wsCloseHandler(ws, code, reason));
    ws.on('error',   (error)        => this.wsErrorHandler(error)           );
    this.sendMessage(ws, 'HELLO', null);
  }

/* Web Socket Event Handlers */

  wsOpenHandler() {
    console.log('[WS-OPEN]');
  }

  wsMessageHandler(ws, message) {
    console.log('[WS-MESSAGE]', message);
    let msg = JSON.parse(message);
    if (msg.msg_type in this.callbacks)
      this.callbacks[msg.msg_type](ws, msg);
    else
      console.log('[WARN] Unhandled Message Type:', msg.msg_type);
  }

  wsCloseHandler(ws, code, reason) {
    console.log('[WS-CLOSE]', code, reason);
    let index = this.monitors.indexOf(ws);
    if (index != -1) this.monitors.splice(index, 1);
  }

  wsErrorHandler(error) {
    console.log('[WS-ERROR]', error);
  }

/* Message Handlers */

registerMessageHandlers() {
  this.callbacks['HELLO'] = (ws, msg) => { this.addSocket(ws, msg); };
  this.callbacks['UPLOAD_DIR'] = (ws, msg) => { this.updateClientFiles(msg); };
  this.callbacks['FS_SEND'] = (ws, msg) => { this.relayJob(msg.data); };
}

addSocket(ws, msg) {
  console.log('Adding Socket %s (%s)', msg.node_id, msg.node_type);
  if (['client', 'dtn'].includes(msg.node_type)) {
    this.sources[msg.node_id] = { type: msg.node_type };
    this.sendSourceUpdate(this.monitors);
    //this.sendDataRequest(ws);
  }
  if (msg.node_type == 'monitor') {
    this.monitors.push(ws);
    this.sendSourceUpdate([ws]);
  }
  this.sockets[msg.node_id] = ws;
}

updateClientFiles(msg) {
  this.sources[msg.node_id]['files'] = msg.data;
  this.sendSourceUpdate(this.monitors);
}

relayJob(msg) {
  let data = { file_name: msg.file_name, rcpt: msg.rcpt };
  this.sendMessage(this.sockets[msg.client], 'FS_SEND', data);
}

/* Utility Methods */

  sendDataRequest(ws) {
    let data = { interval: this.config.interval };
    this.sendMessage(ws, 'DATA_REQ', data);
  }

  sendSourceUpdate(monitors) {
    for (let ws of monitors) this.sendMessage(ws, 'SRC_UPDATE', this.sources);
  }

  sendMessage(ws, msg_type, data) {
    let message = {
      node_id: this.config.node_id,
      node_type: this.config.node_type,
      msg_type: msg_type,
      timestamp: Date.now(),
      data: data
    };

    console.log('[SENDING]', message);
    ws.send(JSON.stringify(message));
  }
}

dtn_agent = new AgentDemoSink('sink.json');
dtn_agent.init();
