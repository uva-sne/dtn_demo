const TOPO = {
  nodes: [
    // ID, Label, Type, [x, y]
    [ 'DTN-A', 'DTN-A', 'dtn', [400, 200]],
    [ 'A1', 'A1', 'client', [300, 100]],
    [ 'A2', 'A2', 'client', [400, 100]],
    [ 'A3', 'A3', 'client', [500, 100]],
    [ 'DTN-B', 'DTN-B', 'dtn', [200, 600]],
    [ 'B1', 'B1', 'client', [100, 700]],
    [ 'B2', 'B2', 'client', [200, 700]],
    [ 'B3', 'B3', 'client', [300, 700]],
    [ 'DTN-C', 'DTN-C', 'dtn', [600, 600]],
    [ 'C1', 'C1', 'client', [500, 700]],
    [ 'C2', 'C2', 'client', [600, 700]],
    [ 'C3', 'C3', 'client', [700, 700]]
  ],
  edges: [
    // id,id  order does not matter
    ["DTN-A","A1"],
    ["DTN-A","A2"],
    ["DTN-A","A3"],
    ["DTN-B","B1"],
    ["DTN-B","B2"],
    ["DTN-B","B3"],
    ["DTN-C","C1"],
    ["DTN-C","C2"],
    ["DTN-C","C3"],
    ["DTN-A","DTN-B"],
    ["DTN-B","DTN-C"],
    ["DTN-C","DTN-A"]
  ]
};

const CONFIG = {
  "node_id": "Demo Monitor",
  "node_type": "monitor",
  "sink_host": "10.127.127.84",
  "sink_port": 3002,
  "restart_interval": 5000
};

class Transfer {
  constructor() {
    this.src = null; // node id
    this.dst = null; // node id
    this.progress = 0;
    this.complete = False;
  }
}

class File {
  constructor() {
    this.id = null;
    this.name = null;
    this.size = null;
    this.speed = 0;
    this.transfers = [];
    this.complete = False;
  }
}

class Job {
  constructor() {
    this.id = null;
    this.type = null; // direct|local|dtn , number of transfers
    this.files = [];
    // Get speed by adding up all files
  }

  static get TYPE() {
    return {
      DTN: 3,
      LOCAL: 2,
      DIRECT: 1
    }
  }
}

class Demo {
  constructor(topo) {
    let edges = [];
    this.topo = new Topology(topo);
    this.jobs = {};
  }
  init() {
    let animator = new Animator(this.topo, 'demo_main');
    animator.animate();
  }

  eventHandler(event) {
    if (event.type == 'JOB_NOTIFICATION') this.initFile(event);
    else if (event.type == 'TRANSFER_UPDATE') this.updateFile(event);
    else console.log('ERROR: Unhandeled Event');
  }

  initJob(notification) {
    this.files[file.id] = {
      src: file.src,
      dst: file.dst,
      id: file.id,
      name: file.name,
      size: file.size,
      progress: 0,
      speed: 0
    };
    console.log('File transfer start...');
  }

  updateFile(file) {
    // Find and update the apropriate flow
    this.files[file.id].progress = file.progress;
    this.files[file.id].speed = file.speed;
  }

  endFile(file) {
    this.files[file.id].progress = 1;
    this.files[file.id].speed = 0;
    console.log('File transfer complete...')
  }

  test() {
    this.eventHandler({
      type: 'INIT_FILE_TRANSFER',
      src: 'B1',
      dst: 'DTN-B',
      id: '818a9a90-e3bf-e3c7-2af0-8c4c377e5d53',
      name: 'test.dat',
      size: 10737418240
    });

    this.eventHandler({
      type: 'FILE_STATS',
      id: '818a9a90-e3bf-e3c7-2af0-8c4c377e5d53',
      progress: 0.25, // 0 - 1
      speed: 1000000 // B/s
    });

    this.eventHandler({
      type: 'FILE_STATS',
      id: '818a9a90-e3bf-e3c7-2af0-8c4c377e5d53',
      progress: 0.75, // 0 - 1
      speed: 100000000 // B/s
    });

    this.eventHandler({
      type: 'END_FILE_TRANSFER',
      id: '818a9a90-e3bf-e3c7-2af0-8c4c377e5d53',
    });
  }
}

function genEdges(TOPO) {

}

function main() {
  demo = new Demo(TOPO);
  demo.init();
  //console.log(demo);

  let monitor = new DemoMonitor(CONFIG);
  monitor.init();
}

window.onload = main;
