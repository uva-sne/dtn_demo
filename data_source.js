/*
This is a component of the DTN Agent Demo. It will send infromation about the
apparent size of files in the data directory to a sink over a websocket. The
configuration must provide the data direcory, hostname/port of the sink and a
unique identifer. Data will be sent at an interval determined by the sink.
*/

const WebSocket = require('ws');
const fs = require('fs'); // Read Config file
const os = require('os'); // Get System Hostname
const { exec } = require('child_process');

class AgentDemoDataSource {
  constructor(config_file_name) {
    this.config = JSON.parse(fs.readFileSync('config/' + config_file_name, 'utf8'));
    this.ws = null;
    this.interval_id = 0;
  }

  init() {
    console.log('[INIT]');
    var url = `ws://${this.config.sink_host}:${this.config.sink_port}`;
    this.ws = new WebSocket(url);
    this.ws.on('open',    ()             => this.openHandler()             );
    this.ws.on('message', (data)         => this.messageHandler(data)      );
    this.ws.on('close',   (code, reason) => this.closeHandler(code, reason));
    this.ws.on('error',   (error)        => this.errorHandler(error)       );
  }

  openHandler() {
    console.log('[OPEN]');
    let data = { id: this.config.id, type: 'data_source_hello' };
    console.log('[SENDING]', data);
    this.ws.send(JSON.stringify(data));
  }

  messageHandler(message) {
    console.log('[MESSAGE]', message);

    let data = JSON.parse(message);
    switch (data.type) {
      case 'data_request':
        this.initInterval(data.interval);
    }
  }

  closeHandler(code, reason) {
    console.log('[CLOSE]', code, reason);
    this.stopInterval();
    if (this.config.restart_interval) this.restart();
  }

  errorHandler(error) {
    console.log('[ERROR]', error);
  }

  initInterval(interval) {
    if (this.interval_id) this.stopInterval();
    this.interval_id = setInterval(() => this.sendStats(), interval);
  }

  stopInterval() {
    clearInterval(this.interval_id);
    this.interval_id = 0;
  }

  sendStats() {
    let options = { cwd: this.config.data_dir, encoding: 'utf8' };
    exec('du -b *', options, (error, stdout, stderr) => {
      let data = { id: this.config.id, type: 'data', timestamp: Date.now(), stats: {} };
      let lines = stdout.trim().split('\n');

      for (let line of lines) {
        let [size, file_name] = line.split('\t');
        data.stats[file_name] = size;
      }
      console.log('[SENDING]', data);
      this.ws.send(JSON.stringify(data));
    });
  }

  restart(delay) {
    console.log('[RESTART]', this.config.restart_interval);
    setTimeout(() => this.init(), this.config.restart_interval);
  }
}

dtn_agent = new AgentDemoDataSource(os.hostname() + '.json');
dtn_agent.init();
