const DEFAULTS = {
  node: {
    dtn: {
      r: 20
    },
    client: {
      r: 10
    }
  },
  edge: {
    width: 4,
    color: '#000'
  },
  traffic: {
    dash_pattern: [4,60],
    width: 2,
    color: '#fff'
  },
  grid: {
    color: '#ddd',
    offset: 100,
    line_width: 1
  }
};

class Grid {
  constructor() {
    this.color = DEFAULTS.grid.color;
    this.offset = DEFAULTS.grid.offset;
    this.line_width = DEFAULTS.grid.line_width;
  }

  draw(ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = this.color;
    ctx.lineWidth = this.line_width;

    for (let y = 0; y <= ctx.canvas.height; y += this.offset) {
      ctx.moveTo(0, y);
      ctx.lineTo(ctx.canvas.width, y);
    }
    for (let x = 0; x <= ctx.canvas.width; x += this.offset) {
      ctx.moveTo(x, 0);
      ctx.lineTo(x, ctx.canvas.height);
    }
    ctx.stroke();
    ctx.restore();
  }
}

class Node {
  constructor(id, label, type, xy) {
    this.id = id;
    this.label = label;
    this.type = type;
    this.x = xy[0];
    this.y = xy[1];
    this.r = DEFAULTS.node[type].r;
  }

  get xy() { return [this.x,this.y]; }
  set xy(v) { this.x = v[0]; this.y = v[1]; }

  update(delta) {}

  draw(ctx) {
    ctx.save();
    ctx.beginPath();
    ctx.fillStyle = '#f00';
    ctx.strokeStyle = '#000';
    ctx.lineWidth = 2;
    ctx.arc(this.x, this.y, this.r, 0, Math.PI * 2);
    ctx.fill();
    ctx.stroke();
    ctx.lineWidth = 4;
    ctx.miterLimit = 1;
    ctx.font = '15px sans-serif';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'top';
    ctx.fillStyle = '#000';
    ctx.strokeStyle = '#fff';
    ctx.strokeText(this.label, this.x, this.y + (this.r * 1.2));
    ctx.fillText(this.label, this.x, this.y + (this.r * 1.2));
    ctx.restore();
  }
}

// Unidirectional Data Traffic
class Traffic {
  constructor(src, dst) {
    this.src = src;
    this.dst = dst;
    this.dash_offset = 0;
    this.dash_speed = 0.02;
    this.dash_pattern = DEFAULTS.traffic.dash_pattern;
    this.transfers = {};
  }

  update(delta) {
    this.dash_offset += this.dash_speed * delta;
  }

  draw(ctx) {
    let [x1, y1] = this.src.xy;
    let [x2, y2] = this.dst.xy;

    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = DEFAULTS.traffic.color;
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.lineWidth = DEFAULTS.traffic.width;
    ctx.setLineDash(this.dash_pattern);
    ctx.lineDashOffset = this.dash_offset;
    ctx.stroke();
    ctx.restore();
  }
}

class Edge {
  constructor(node1, node2) {
    this.endpoints = [node1, node2];
    this.traffic = [new Traffic(node1, node2), new Traffic(node2, node1)];
  }

  update(delta) {
    for (let traffic of this.traffic) traffic.update(delta);
  }

  draw(ctx) {
    //var [x1, y1, x2, y2] = offsetLine(src.x, src.y, dst.x, dst.y, 4);
    let [node1, node2] = this.endpoints.values();
    let [x1, y1] = this.endpoints[0].xy;
    let [x2, y2] = this.endpoints[1].xy;

    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = DEFAULTS.edge.color;
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.lineWidth = DEFAULTS.edge.width;
    ctx.stroke();
    ctx.restore();

    for (let traffic of this.traffic) traffic.draw(ctx);
  }
}

class Topology {
  constructor(topo) {
    this.nodes = new Set();
    this.edges = new Set();

    for (let node of topo.nodes) {
      this.addNode(...node);
    }

    for (let edge of topo.edges) {
      this.addEdge(...edge);
    }
  }

  addNode(id, label, type, xy) {
    let node = new Node(id, label, type, xy);
    this.nodes.add(node);
  }

  getNode(id) {
    for (let node of this.nodes) {
      if (node.id == id) return node;
    }
    return null;
  }

  getEdge(node1, node2) {
    for (let edge of this.edges) {
      if (edge.endpoints[0] == node1 & edge.endpoints[1] == node2) return edge;
      if (edge.endpoints[1] == node1 & edge.endpoints[0] == node2) return edge;
    }
    return null;
  }

  getTraffic(src_id, dst_id) {
    let src = getNode(src_id);
    let dst = getNode(dst_id);
    let edge = this.getEdge(src, dst);
    if (edge) for (let traffic of edge.traffic)
      if (traffic.src == src & traffic.dst == dst) return traffic;
    return null;
  }

  addEdge(src_id, dst_id, type) {
    let src = this.getNode(src_id);
    let dst = this.getNode(dst_id);
    let edge = new Edge(src,dst);
    this.edges.add(edge);
  }
}

class Animator {
  constructor(topo, canvas_id) {
    this.prev_ts = Date.now();
    this.topo = topo;
    this.canvas = document.getElementById(canvas_id);
    this.ctx = this.canvas.getContext('2d');
    this.grid = new Grid();
  }

  animate() {
    this.delta = Date.now() - this.prev_ts;
    this.prev_ts += this.delta;

    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    if (this.grid) this.grid.draw(this.ctx);

    for (let edge of this.topo.edges) {
      edge.update(this.delta);
      edge.draw(this.ctx);
    }
    for (let node of this.topo.nodes) {
      node.update(this.delta);
      node.draw(this.ctx);
    }
    window.requestAnimationFrame(() => this.animate());
  }
}
