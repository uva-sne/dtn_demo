/*
Demo Client
Will initate transfers at the command of the monitor (via controller).
Transfers can be through FileSender or Direct.
Will send event information to Controller for monitoring. (?)
Config will provde user information
- username
- user_email
- API key
Recipient and file_name will be in INIT command
On init provide user and available file details to the controller
*/
const fs = require('fs'); // Read Config file
const cp = require('child_process');
const DemoDataSource = require('./modules/DemoDataSource.js').DemoDataSource;
const DEFAULT_CONFIG_FILE = './config/client.js';

class DemoClient extends DemoDataSource {
  constructor(config) {
    super(config);
    this.registerMessageHandlers();
    this.ws_init();
  }

  registerMessageHandlers() {
    this.callbacks['HELLO'] = (msg) => { this.sendUploadList() };
    this.callbacks['FS_SEND'] = (msg) => { this.submitJob(msg.data); };
    this.callbacks['FS_RECV'] = (msg) => { this.downloadFile(msg.data); };
    this.callbacks['DIRECT_XFER'] = (msg) => { /* Perform Direct Transfer */ };
    this.callbacks['DATA_REQ'] = (msg) => { this.startDataSource(msg.interval); };
    this.callbacks['DATA_HALT'] = (msg) => { this.stopDataSource(); };
  }

  submitJob(data) {
    this.sendMessage('UPLOAD_START', { file_name: data.file_name, rcpt: data.rcpt });
    let cmd = 'python3';
    let args = [
      this.config.fs_cli_path,
      '-u', this.config.fs_user,
      '-e', this.config.fs_email,
      '-a', this.config.fs_apikey,
      '-r', data.rcpt,
      '-v', this.config.upload_dir + '/' + data.file_name
    ];
    console.log("[DEBUG] Executing", cmd, ...args);
    cp.execFile(cmd, args, null, (error, stdout, stderr) => {
      this.sendMessage('UPLOAD_END', { file_name: data.file_name, rcpt: data.rcpt });
      console.log('[EXEC]');
      console.log('ERROR:', error);
      console.log('STDOUT:', stdout);
      console.log('STDERR:', stderr);
    });
  }

  downloadFile(data) {
    this.sendMessage('DOWNLOAD_START', { uid: data.uid });
    let cmd = 'wget';
    let args = [
      '--no-check-certificate',
      '-O', this.config.data_dir + '/' + data.uid,
      data.url
    ];
    console.log("[DEBUG] Executing", cmd, ...args);
    cp.execFile(cmd, args, null, (error, stdout, stderr) => {
      this.sendMessage('DOWNLOAD_END', { uid: data.uid });
      console.log('[EXEC]');
      console.log('ERROR:', error);
      console.log('STDOUT:', stdout);
      console.log('STDERR:', stderr);
    });
  }

  sendUploadList() {
    let files = this.getDirList(this.config.upload_dir);
    this.sendMessage('UPLOAD_DIR', files);
  }
}

/*
function test0(client) {
  let data = {
    url: "https://10.127.127.84/filesender/download.php?token=49e7fb30-0151-4aa8-905d-40beac0ed99b&files_ids=10",
    uid: "4b19105f-c35f-43c9-a2f3-64cccf9994b4"
  }
  setTimeout(() => { client.downloadFile(data); }, 5000);
}

function test(client) {
  let data = {
    file_name: "100MiB.dat",
    rcpt: "tony@example.com"
  }
  setTimeout(() => { client.submitJob(data); }, 5000);
}
*/
function main() {
  let index = process.argv.indexOf("-c", 2) + 1;
  let file = (index) ? process.argv[index] : DEFAULT_CONFIG_FILE;
  let config = require(file);
  let client = new DemoClient(config);
}

main();
