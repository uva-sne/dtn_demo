/*

*/

// TODO: Allow config to be changed in the UI


class DemoMonitor {
  constructor(config) {
    this.config = config;
    this.ws = null;
    this.interval_id = 0;
    this.callbacks = {};
    this.sources = null;
  }

  init() {
    console.log('[INIT]');
    this.registerMessageHandlers();
    this.init_html();
    let url = `ws://${this.config.sink_host}:${this.config.sink_port}`;
    this.ws = new WebSocket(url);
    this.ws.onopen = (event) => this.openHandler(event); // ('open',    ()             => this.openHandler()             );
    this.ws.onmessage = (event) => this.messageHandler(event); //on('message', (data)         => this.messageHandler(data)      );
    this.ws.onclose = (event) => this.closeHandler(event); //('close',   (code, reason) => this.closeHandler(code, reason));
    this.ws.onerror = (event) => this.errorHandler(event); //('error',   (error)        => this.errorHandler(error)       );
  }

  init_html() {
    let client_select = document.getElementById('client_select');
    client_select.oninput = this.updateFileSelect.bind(this);

    let job_submit = document.getElementById('job_submit');
    job_submit.onclick = this.submitJob.bind(this);
  }

/* WebSocket Event Handelers */

  openHandler(event) {
    console.log('[OPEN]');
    this.sendMessage('HELLO', null);
  }

  messageHandler(event) {
    console.log('[MESSAGE]', event.data);
    let msg = JSON.parse(event.data);
    if (msg.msg_type in this.callbacks)
      this.callbacks[msg.msg_type](msg);
    else
      console.log('[WARN] Unhandled Message Type:', msg.msg_type);
  }

  closeHandler(event) {
    console.log('[CLOSE]', event.code, event.reason);
    if (this.config.restart_interval) this.restart();
  }

  errorHandler(event) {
    console.log('[ERROR]', event);
  }

/* Message Handlers */

  registerMessageHandlers() {
    this.callbacks['HELLO'] = (msg) => { /* ??? */ };
    this.callbacks['SRC_UPDATE'] = (msg) => { this.updateSources(msg.data); };
  }

  updateSources(sources) {
    this.sources = sources;
    let clients = Object.keys(sources).filter(source => sources[source].type == 'client');
    this.updateClientSelect(clients);
  }

/* Other Stuff */

  updateClientSelect(clients) {
    let client_select = document.getElementById('client_select');
    let client_options = "";

    for (let client of clients) {
      client_options += `<option value="${client}">${client}</option>\n`;
    }
    client_select.innerHTML = client_options;
    this.updateFileSelect();
  }

  updateFileSelect() {
    let client_select = document.getElementById('client_select');
    let file_select = document.getElementById('file_select');
    let client = client_select.item(client_select.selectedIndex);
    let files = (client) ? this.sources[client.value].files : null;
    let file_options = "";

    for (let file in files) {
      file_options += `<option value="${file}">${file} (${files[file]} B)</option>\n`;
    }
    file_select.innerHTML = file_options;
  }

  submitJob() {
    let client_select = document.getElementById('client_select');
    let client = client_select.item(client_select.selectedIndex).value;
    let file_select = document.getElementById('file_select');
    let file_name = file_select.item(file_select.selectedIndex).value;
    let rcpt = document.getElementById('rcpt').value;
    let data = { client: client, rcpt: rcpt, file_name: file_name };
    this.sendMessage('FS_SEND', data);
  }

  sendMessage(message_type, data) {
    let message = {
      node_id: this.config.node_id,
      node_type: this.config.node_type,
      msg_type: message_type,
      timestamp: Date.now(),
      data: data
    };

    console.log('[SENDING]', message);
    this.ws.send(JSON.stringify(message));
  }

  restart(delay) {
    console.log('[RESTART]', this.config.restart_interval);
    setTimeout(() => this.init(), this.config.restart_interval);
  }
}
