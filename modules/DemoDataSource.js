/*
Required Configuration Settings:
"ws_host": "10.127.127.84",
"ws_port": "3002",
"node_id": "Alice",
"node_type": "client",
"restart_delay": 5000,
*/

const WebSocket = require('ws');
const fs = require('fs');

exports.DemoDataSource = class {
  constructor(config) {
    this.config = config;
    this.callbacks = {};
    this.interval_id = 0;
  }

// WebSocket Methods
  ws_init() {
    console.log('[INIT]');
    var url = `ws://${this.config.ws_host}:${this.config.ws_port}`;
    this.ws = new WebSocket(url);
    this.ws.on('open',    ()             => this.openHandler()             );
    this.ws.on('message', (data)         => this.messageHandler(data)      );
    this.ws.on('close',   (code, reason) => this.closeHandler(code, reason));
    this.ws.on('error',   (error)        => this.errorHandler(error)       );
  }

  openHandler() {
    console.log('[OPEN]');
    this.sendMessage('HELLO', null);
  }

  messageHandler(message) {
    console.log('[MESSAGE]', message);
    let msg = JSON.parse(message);
    if (msg.msg_type in this.callbacks)
      this.callbacks[msg.msg_type](msg);
    else
      console.log('[WARN] Unhandled Message Type:', msg.msg_type);
  }

  closeHandler(code, reason) {
    console.log('[CLOSE]', code, reason);
    if (this.interval_id) this.stopDataSource();
    if (this.config.restart_delay) this.restart();
  }

  errorHandler(error) {
    console.log('[ERROR]', error);
  }

  sendMessage(msg_type, data) {
    let message = {
      node_id: this.config.node_id,
      node_type: this.config.node_type,
      msg_type: msg_type,
      timestamp: Date.now(),
      data: data
    };

    console.log('[SENDING]', message);
    this.ws.send(JSON.stringify(message));
  }

  restart() {
    console.log('[RESTART]', this.config.restart_delay);
    setTimeout(() => this.ws_init(), this.config.restart_delay);
  }

// Data Source Methods
  startDataSource(interval) {
    if (this.interval_id) this.stopDataSource();
    this.interval_id = setInterval(this.sendStatistics.bind(this), interval);
  }

  stopDataSource() {
    clearInterval(this.interval_id);
    this.interval_id = 0;
  }

// Utility Methods
  getDirList(dir) {
    let dirList = {};
    let files = fs.readdirSync(dir);
    for (let file of files) {
      let stats = fs.statSync(dir + '/' + file);
      dirList[file] = stats.size;
    }
    return dirList;
  }

  sendStatistics() {
    let files = this.getDirList(this.config.data_dir);
    this.sendMessage('DATA_DIR', files);
  }
}


/*
  event(event, ...params) {
    let data = {};

    switch(event) {
      case 'AGNT_RX_IREQ': // <IntRequest>
        data['frontend_type'] = params[0].frontend.type;
        data['recipients'] = params[0].recipients;
        data['files'] = params[0].files;
        break;
      case 'AGNT_TX_XREQ': // <string> peer, <extRequest>
        data['peer'] = params[0];
        data['recipients'] = params[1].recipients;
        data['files']  = params[1].files;
        break
      case 'AGNT_RX_XREQ': // <extRequest>
        data['peer'] = params[0].source;
        data['recipients'] = params[0].recipients;
        data['files']  = params[0].files;
        break;
      //case 'FRNT_RX_XFER_INIT_RES': transfer_id
      //case 'FRNT_RX_XFER_DONE_RES': transfer_id
      case 'LOOKUP_REQ': // <string> email
        data['email'] = params[0];
        break;
      case 'LOOKUP_RES': // <string> email, <string> dtn
        data['email'] = params[0];
        data['dtn'] = params[1];
        break;
      case 'XFER_SET_INIT': // <transfet_set>
        data['peer'] = params[0].peer;
        data['files'] = params[0].files;
        break;
      case 'XFER_FILE_INIT': // <transfet_set>, <file>
        data['peer'] = params[0].peer;
        data['file'] = params[1];
        break;
      case 'XFER_FILE_DONE': // <transfet_set>, <file>
        data['peer'] = params[0].peer;
        data['file'] = params[1];
        break;
    }

    this.sendEvent(event, data);
  }
*/
